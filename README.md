# serial_demo

#### 介绍
eTS项目hap包中实现串口访问，通过JS接口开放给上层应用使用

#### 软件架构
软件架构说明


#### 安装教程

1.  推送serial_service：进入serial_service_so目录，执行serial_push.sh脚本将服务so与配置文件推送至开发板
2.  重启开发板，检查serialport_service服务
3.  编译serial_demo，生成hap包安装到开发板

#### 使用说明

1.  master分支需要使用serial_service
2.  建议使用v3.2.1分支，它可以直接使用libnative_serialport.so，已经移除了对serial_service的依赖


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
