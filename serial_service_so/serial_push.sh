#!/bin/bash

filename=`pwd`

echo ${filename}

hdc file send ${filename}/serial_service.cfg /etc/init/serial_service.cfg
hdc file send ${filename}/serialport_service.xml /system/profile/serialport_service.xml
hdc file send ${filename}/libserialport_service.z.so /system/lib/libserialport_service.z.so
hdc shell chmod 0777 /system/lib/libserialport_service.z.so
