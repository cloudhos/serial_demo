/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_SERIALPORT_LOG_WRAPPER_H
#define OHOS_SERIALPORT_LOG_WRAPPER_H

#include <string>

#include <Hilog/log.h>

#ifdef LOG_DOMAIN
#undef LOG_DOMAIN
#endif
#define LOG_DOMAIN 0xD002B03

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "SerialportSubsystem"

namespace OHOS {
namespace SerialPort {

#define CONFIG_SERIAL_HILOG
#ifdef CONFIG_SERIAL_HILOG

#define OHOS_DEBUG
#ifndef OHOS_DEBUG
#define PRINT_OHOS_HILOG(op, fmt, ...) (void)op(LOG_APP, fmt, ##__VA_ARGS__)
#else
    // Gets the raw file name of the file.
    // This function is a function executed by the compiler, that is,
    // it has been executed at compile time. When the program runs,
    // it directly refers to the value calculated by this function
    // and does not consume CPU for calculation.
    inline constexpr const char *GetRawFileName(const char *path)
    {
        char ch = '/';
        const char *start = path;
        // get the end of the string
        while (*start++) {
            ;
        }
        while (--start != path && *start != ch) {
            ;
        }
        return (*start == ch) ? ++start : path;
    }

#define PRINT_OHOS_HILOG(op, fmt, ...)            \
    (void)op(LOG_APP, "[%{public}s-(%{public}s:%{public}d)] " fmt, \
        __FUNCTION__, OHOS::SerialPort::GetRawFileName(__FILE__), __LINE__, ##__VA_ARGS__)

#endif // OHOS_DEBUG

#define SERIALPORT_LOGE(fmt, ...) PRINT_OHOS_HILOG(OH_LOG_ERROR, fmt, ##__VA_ARGS__)
#define SERIALPORT_LOGW(fmt, ...) PRINT_OHOS_HILOG(OH_LOG_WARN, fmt, ##__VA_ARGS__)
#define SERIALPORT_LOGI(fmt, ...) PRINT_OHOS_HILOG(OH_LOG_INFO, fmt, ##__VA_ARGS__)
#define SERIALPORT_LOGF(fmt, ...) PRINT_OHOS_HILOG(OH_LOG_FATAL, fmt, ##__VA_ARGS__)
#define SERIALPORT_LOGD(fmt, ...) PRINT_OHOS_HILOG(OH_LOG_DEBUG, fmt, ##__VA_ARGS__)

#endif // CONFIG_SERIAL_HILOG
} // namespace SerialPort
} // namespace OHOS

#endif // OHOS_SERIALPORT_LOG_WRAPPER_H