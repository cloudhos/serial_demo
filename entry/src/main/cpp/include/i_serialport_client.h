/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef I_SERIALPORT_CLIENT_H
#define I_SERIALPORT_CLIENT_H

#include <string>
#include <vector>

#include "serialport_types.h"
#include "serial_callback_base.h"

namespace OHOS {
namespace SerialPort {
class ISerialPortClient {
public:
    virtual bool IsConnect() const = 0;
    virtual bool SendCallbackEvent(const std::shared_ptr<SerialCallbackBase> &callback, int32_t delayTime) = 0;
    virtual void RegisterAsyncCallback(const std::string &name, const std::shared_ptr<SerialCallbackBase> &callback) = 0;
    virtual void UnregisterAsyncCallback(const std::string &name) = 0;
    virtual int32_t SetOptions(const std::string serialId, const SerialOptions &opts) = 0;
    virtual int32_t OpenSerial(const std::string serialId) = 0;
    virtual int32_t CloseSerial(const std::string serialId) = 0;
    virtual int32_t SendData(const std::string serialId, const uint8_t *buffer, uint32_t length) = 0;
    virtual int32_t RecvData(const std::string serialId, std::vector<uint8_t> &buffer, uint32_t timeout) = 0;
    virtual int32_t Transmit(const std::string serialId, const uint8_t *cmd, uint32_t length,
        std::vector<uint8_t> &buffer, uint32_t timeout) = 0;
    virtual int32_t SetAsyncRead(const std::string serialId, uint8_t flag) = 0;
    virtual int32_t ClearBuffer(const std::string serialId, uint8_t selector) = 0;

    virtual int32_t SetGPIODirection(int32_t portNo, bool dirIn) = 0;
    virtual int32_t SetGPIOValue(int32_t portNo, int32_t value) = 0;
    virtual int32_t GetGPIOValue(int32_t portNo, int32_t &value) = 0;
};
extern "C" ISerialPortClient * get_serial_client();
} // namespace SerialPort
} // namespace OHOS
#endif // I_SERIALPORT_CLIENT_H
