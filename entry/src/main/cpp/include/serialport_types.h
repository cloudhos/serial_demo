/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SERIALPORT_TYPES_H
#define SERIALPORT_TYPES_H

#include <cstdint>

namespace OHOS {
namespace SerialPort {
static const int32_t SERIAL_PORT_SERVICE_ABILITY_ID = 4076;
static const uint8_t SERIAL_READ_FLAG_ON = 0x01;
static const uint32_t EVENT_CALLBACK = 1100;
static const int32_t MIN_BUFFER_SIZE = 1024;
struct SerialOptions {
    int32_t speeds;
    int32_t bits;
    int32_t events;
    int32_t stops;
};

} // namespace SerialPort
} // namespace OHOS
#endif // SERIALPORT_TYPES_H