/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef SERIAL_ASYNC_CALLBACK_H
#define SERIAL_ASYNC_CALLBACK_H

#include <mutex>
#include <string>
#include <queue>

#include <node_api.h>

#include "i_serialport_client.h"
#include "serial_callback_base.h"

namespace OHOS {
namespace SerialPort {
struct SerialData {
    uint32_t length;
    char data[1];
};

class SerialAsyncCallback: public SerialCallbackBase {
public:
    SerialAsyncCallback() = default;
    ~SerialAsyncCallback();

    void OnCallBackEvent() override;
    void OnRecvData(const uint8_t *buffer, uint32_t length) override;

    int32_t OnUvWork();

    void SetEnv(const std::string &name, napi_env env, napi_value func);

protected:
    napi_env env_ = 0;
    std::mutex mutex_;
    std::queue<SerialData *> buffer_;
    std::string name_;
    napi_ref funcRef_ = 0;
    bool busy_ = false;
};

} // namespace SerialPort
} // namespace OHOS
#endif // SERIAL_ASYNC_CALLBACK_H
