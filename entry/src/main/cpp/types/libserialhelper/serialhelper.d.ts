/*
 * Copyright (C) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {AsyncCallback, Callback} from "basic";

/**
 * Provides methods related to serialport services.
 *
 * @since 7
 * @syscap SystemCapability.Miscservices.SerialportService
 */
declare namespace serialHelper {
/**
 * Set serial port options.
 * @param dev Indicates the serial port dev.
 * @param speeds baud rate.
 * @param bits 7/8.
 * @param events 'O'/'E'/'N'
 * @param stops 1/2
 */
    function setOptions(dev:string, speeds:number, bits:number, events:number, stops: number, callback: AsyncCallback<void>): void;
    function setOptions(dev:string, speeds:number, bits:number, events:number, stops: number): Promise<void>;

    /**
     * Open serial port.
     * @param dev Indicates the serial port dev.
     */
    function openSerial(dev:string, callback: AsyncCallback<void>): void;
    function openSerial(dev:string): Promise<void>;

    /**
     * Close serial port.
     * @param dev Indicates the serial port dev.
     */
    function closeSerial(dev:string, callback: AsyncCallback<void>): void;
    function closeSerial(dev:string): Promise<void>;

    /**
     * tcflush serial port.
     * @param dev Indicates the serial port dev.
     * @param selector 0 in 1 out 2 in&out.
     */
    function clearBuffer(dev:string, selector:number, callback: AsyncCallback<void>): void;
    function clearBuffer(dev:string, selector:number): Promise<void>;

    /**
     * Send data to serial port.
     * @param dev Indicates the serial port dev.
     * @param data.
     */
    function sendData(dev:string, data:Uint8Array, callback: AsyncCallback<void>): void;
    function sendData(dev:string, data:Uint8Array): Promise<void>;

    /**
     * read data from serial port.
     * @param dev Indicates the serial port dev.
     * @param timeout
     */
    function recvData(dev:string, timeout:number, callback: AsyncCallback<Uint8Array>): void;
    function recvData(dev:string, timeout:number): Promise<Uint8Array>;

    /**
     * transmit Send and Read data
     * @param dev Indicates the serial port dev.
     * @param cmd Indicates the command.
     * @param timeout
     * @param callback Returns the Uint8Array
     */
    function transmit(dev:string, cmd: Uint8Array, timeout: number, callback: AsyncCallback<Uint8Array>): void;
    function transmit(dev:string, cmd: Uint8Array, timeout: number): Promise<Uint8Array>;

    /**
     * on/off serial data
     * @param type Indicates the serial port dev.
     * @param callback serial data
     */
    function on(type: '/dev/ttyXRUSB0', callback: Callback<Uint8Array>): void;
    function on(type: '/dev/ttyXRUSB1', callback: Callback<Uint8Array>): void;
    function on(type: '/dev/ttyXRUSB2', callback: Callback<Uint8Array>): void;
    function on(type: '/dev/ttyXRUSB3', callback: Callback<Uint8Array>): void;
    function off(type: '/dev/ttyXRUSB0'): void;
    function off(type: '/dev/ttyXRUSB1'): void;
    function off(type: '/dev/ttyXRUSB2'): void;
    function off(type: '/dev/ttyXRUSB3'): void;

    /**
     * Set GPIO Direction.
     *
     * @param portNo Gpio number.
     * @param dirIn Is it an input port.
     * @permission None
     */
    function setGPIODirection(portNo:number, dirIn:boolean, callback: AsyncCallback<void>): void;
    function setGPIODirection(portNo:number, dirIn:boolean): Promise<void>;

    /**
    * Set GPIO Value.
    *
    * @param portNo Gpio number.
    * @param value Gpio value, 0 or 1.
    * @permission None
    */
    function setGPIOValue(portNo:number, value:number, callback: AsyncCallback<void>): void;
    function setGPIOValue(portNo:number, value:number): Promise<void>;

    /**
    * Get GPIO Value.
    *
    * @param portNo Gpio number.
    * @param callback Returns gpio value of portNo, 0 or 1.
    * @permission None
    */
    function getGPIOValue(portNo:number, callback: AsyncCallback<number>): void;
    function getGPIOValue(portNo:number): Promise<number>;
}

export default serialHelper;